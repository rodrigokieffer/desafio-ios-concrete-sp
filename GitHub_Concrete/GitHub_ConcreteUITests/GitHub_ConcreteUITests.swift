//
//  GitHub_ConcreteUITests.swift
//  GitHub_ConcreteUITests
//
//  Created by Rodrigo Kieffer on 9/9/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import XCTest

class GitHub_ConcreteUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let app = XCUIApplication()
        
        //Repositories View
        var tableElements = app.tables.element
        let existsPredicate = NSPredicate(format: "exists == true")

        self.expectationForPredicate(existsPredicate, evaluatedWithObject: tableElements, handler: nil)
        self.waitForExpectationsWithTimeout(30, handler: nil)
        
        var firstCell = app.tables.cells.elementBoundByIndex(0)
        
        self.expectationForPredicate(existsPredicate, evaluatedWithObject: firstCell, handler: nil)
        self.waitForExpectationsWithTimeout(20, handler: nil)

        firstCell.tap() //Call Pull Request

        //Pull Request View
        tableElements = app.tables.element
        self.expectationForPredicate(existsPredicate, evaluatedWithObject: tableElements, handler: nil)
        self.waitForExpectationsWithTimeout(30, handler: nil)

        firstCell = app.tables.cells.elementBoundByIndex(0)
        
        self.expectationForPredicate(existsPredicate, evaluatedWithObject: firstCell, handler: nil)
        self.waitForExpectationsWithTimeout(20, handler: nil)
        
        firstCell.tap()
    }
    
}
