//
//  Repository.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/9/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit
import Mantle

class Repository: MTLModel, MTLJSONSerializing {

    var repositoryName          = ""
    var repositoryDescription   = ""
    var numberOfForks           = 0
    var numberOfStars           = 0
    var pullRequestUrl          = ""
    var owner                   = Owner()
    
    class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["repositoryName"        : "name",
                "repositoryDescription" : "description",
                "numberOfForks"         : "forks_count",
                "numberOfStars"         : "stargazers_count",
                "pullRequestUrl"        : "pulls_url",
                "owner"                 : "owner"]
    }
    
    class func ownerJSONTransformer() -> NSValueTransformer {
        return MTLJSONAdapter.dictionaryTransformerWithModelClass(Owner.self)
    }

    
}
