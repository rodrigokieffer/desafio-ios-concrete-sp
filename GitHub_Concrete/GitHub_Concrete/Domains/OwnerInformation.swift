//
//  OwnerInformation.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/9/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit
import Mantle

class OwnerInformation: MTLModel, MTLJSONSerializing {

    var login       = ""
    var avatarUrl   = ""
    var name        = ""
    
    class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["login"     : "login",
                "avatarUrl" : "avatar_url",
                "name"      : "name"]
    }
    
}
