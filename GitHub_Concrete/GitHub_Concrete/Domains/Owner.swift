//
//  Owner.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/9/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit
import Mantle

class Owner: MTLModel, MTLJSONSerializing {
    
    var url         = ""
    var fullName    = ""
    var login       = ""
    var avatarUrl   = ""
    var information = OwnerInformation()
    
    class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["url"       : "url",
                "login"     : "login",
                "avatarUrl" : "avatar_url"]
    }
}
