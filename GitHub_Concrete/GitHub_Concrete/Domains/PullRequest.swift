//
//  PullRequest.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit
import Mantle

class PullRequest: MTLModel, MTLJSONSerializing {

    var title               = ""
    var pullDescription     = ""
    var htmlUrl             = ""
    var owner               = Owner()
    
    class func JSONKeyPathsByPropertyKey() -> [NSObject : AnyObject]! {
        return ["title"             : "title",
                "pullDescription"   : "body",
                "htmlUrl"           : "html_url",
                "owner"             : "user"]
    }
    
    class func userJSONTransformer() -> NSValueTransformer {
        return MTLJSONAdapter.dictionaryTransformerWithModelClass(Owner.self)
    }

}
