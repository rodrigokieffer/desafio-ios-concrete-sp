//
//  PullRequestsTableViewController.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit

class PullRequestsTableViewController: UITableViewController {

    var mainRepository = Repository()
    let cellIdentifier = "pullRequestCellIdentifier"
    var pullRequests = [PullRequest]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = mainRepository.repositoryName
        
        loadPullRequests()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Load Pull Requests
    private func loadPullRequests() {
        let interaction = PullRequestInteractor()
        let pullUrl = mainRepository.pullRequestUrl.stringByReplacingOccurrencesOfString("{/number}", withString: "")
        LoadingHelper.showLoading()
        interaction.loadPullRequests(pullUrl, success: { (pullRequests) in
            LoadingHelper.hideLoading()
            self.pullRequests = pullRequests
            self.tableView.reloadData()
            }, failure: { (error) in
                LoadingHelper.hideLoading()
                AlertHelper.showAlert(AlertDefinitions.Title.Default, message: error.localizedDescription, viewController: self)
        })
    }
    
    // MARK: - Actions
    @IBAction func refreshPullRequestsAction(sender: AnyObject) {
        loadPullRequests()
    }

    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PullRequestTableViewCell

        let pullRequest = pullRequests[indexPath.row]
        cell.labelPullRequestName.text          = pullRequest.title
        cell.labelPullRequestDescription.text   = pullRequest.pullDescription
        cell.labelPullRequestLogin.text         = pullRequest.owner.login
        cell.labelPullRequestFullName.text      = pullRequest.owner.fullName
        
        let url = NSURL(string: pullRequest.owner.avatarUrl)
        cell.imagePullRequestAvatar.sd_setImageWithURL(url)

        return cell
    }

    //MARK: - TableView Delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pullRequest = pullRequests[indexPath.row]
        let htmlUrl = NSURL(string: pullRequest.htmlUrl)!
        
        if UIApplication.sharedApplication().canOpenURL(htmlUrl) {
            UIApplication.sharedApplication().openURL(htmlUrl)
        }
    }
}
