//
//  RepositoriesTableViewController.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/9/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoriesTableViewController: UITableViewController {

    lazy var indexPathSelected = NSIndexPath(forRow: 0, inSection: 0)
    lazy var repositories = [Repository]()
    lazy var page = 1
    let cellIdentifier = "repositoryIdentifier"
    let kSegueToPullRequests = "SegueToPullRequests"
    
    @IBOutlet weak var buttonRefresh: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        page = 1
        loadRepositories(page)
        buttonRefresh.enabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Load Repositories
    private func loadRepositories(page: Int) {
        let interaction = RepositoryInteractor()
        LoadingHelper.showLoading()
        interaction.loadRepositories(page, success: { (repositories) in
            LoadingHelper.hideLoading()
            self.buttonRefresh.enabled = false
            self.repositories.appendContentsOf(repositories)
            self.tableView.reloadData()
        }, failure: { (error) in
            LoadingHelper.hideLoading()
            self.buttonRefresh.enabled = true
            AlertHelper.showAlert(AlertDefinitions.Title.Default, message: error.localizedDescription, viewController: self)
        })
    }
    
    // MARK: - Actions
    @IBAction func refreshRepositoriesAction(sender: AnyObject) {
        loadRepositories(page)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! RepositoryTableViewCell

        let repository = repositories[indexPath.row]
        cell.labelRepositoryName.text           = repository.repositoryName
        cell.labelRepositoryDescription.text    = repository.repositoryDescription
        cell.labelForks.text                    = "\(repository.numberOfForks)"
        cell.labelStars.text                    = "\(repository.numberOfStars)"
        cell.labelUsername.text                 = repository.owner.login
        cell.labelFullname.text                 = repository.owner.fullName
        
        let url = NSURL(string: repository.owner.avatarUrl)
        cell.imageAvatar.sd_setImageWithURL(url)

        if indexPath.row == repositories.count - 1 {
            page = page + 1
            loadRepositories(page)
        }
        
        return cell
    }
    
    //MARK: - TableView Delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let repository = repositories[indexPath.row]
        self.performSegueWithIdentifier(kSegueToPullRequests, sender: repository)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == kSegueToPullRequests {
            let pullRequestViewController = segue.destinationViewController as! PullRequestsTableViewController
            pullRequestViewController.mainRepository = sender as! Repository
        }
    }
}
