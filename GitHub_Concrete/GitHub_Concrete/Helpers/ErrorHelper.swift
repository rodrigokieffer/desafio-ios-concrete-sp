//
//  ErrorHelper.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit

struct Error {
    struct Code {
        static let Undefined            = -1050
        static let SearchRepositories   = -1051
        static let OwnerInformation     = -1052
        static let SearchPullRequests   = -1053
    }
    
    struct Message {
        static let Undefined                = "Ocorreu um problema não identificado, por favor tente mais tarde."
        static let SearchRepositories       = "Nenhum repositório encontrado."
        static let OwnerInformation         = "Nenhuma informação encontrada."
        static let SearchPullRequests       = "Nenhum pull request encontrado."
    }
}


class ErrorHelper: NSObject {
    private static let domainApp = "Desafio iOS"
    
    class func createGenericError(message: String) -> NSError {
        let error = NSError(domain: domainApp, code: Error.Code.Undefined, userInfo: [NSLocalizedDescriptionKey: message])
        
        return error
    }
    
    class func createError(errorCode: Int, message: String) -> NSError {
        let error = NSError(domain: domainApp, code: errorCode, userInfo: [NSLocalizedDescriptionKey: message])
        
        return error
    }    
}
