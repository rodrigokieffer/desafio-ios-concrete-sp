//
//  AlertHelper.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit

struct AlertDefinitions {
    struct Title {
        static let Default  = "Desafio Concrete iOS"
        static let None     = ""
    }
}

class AlertHelper: NSObject {
    
    class func showAlert(title: String, message: String, viewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle:.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
        alertController.addAction(okAction)
        
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    class func showAlert(title: String, message: String, viewController: UIViewController, dismissViewController: Bool) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle:.Alert)

        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: { (action:UIAlertAction!) in
            if dismissViewController {
                viewController.dismissViewControllerAnimated(true, completion: nil)
            }
        })
        alertController.addAction(okAction)
        
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }    
}
