//
//  StubHelper.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit
import OHHTTPStubs

class StubHelper: NSObject {

    func loadRepositories() {
        stub(isHost("api.github.com") && isPath("/search/repositories")) { request in
            return OHHTTPStubsResponse(fileAtPath: OHPathForFile("repositories_mock_page1.json", self.dynamicType)!,
                                       statusCode: 200, headers: ["Content-Type" : "application/json"])
        }
    }
    
    func loadPullRequests() {
        stub(isHost("api.github.com") && isPath("/repos/JakeWharton/butterknife/pulls")) { request in
            return OHHTTPStubsResponse(fileAtPath: OHPathForFile("pullrequests_butterknife_mock.json", self.dynamicType)!,
                                       statusCode: 200, headers: ["Content-Type" : "application/json"])
        }
    }
    
    class func removeAllStubs() {
        OHHTTPStubs.removeAllStubs()
    }
}
