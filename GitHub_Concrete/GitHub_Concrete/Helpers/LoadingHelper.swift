//
//  LoadingHelper.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/11/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit

class LoadingHelper: NSObject {
    private static let loadingView = UIView(frame: UIScreen.mainScreen().bounds)
    private static let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
    
    private class func createLoadingView() {
        loadingView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        
        activityIndicator.alpha = 1.0
        activityIndicator.center = CGPointMake(loadingView.frame.size.width / 2, loadingView.frame.size.height / 2)
        activityIndicator.startAnimating()
        loadingView.addSubview(activityIndicator)
        
        loadingView.bringSubviewToFront(activityIndicator)
    }
    
    class func showLoading() {
        createLoadingView()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        UIApplication.sharedApplication().keyWindow?.addSubview(loadingView)
    }
    
    class func hideLoading() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        activityIndicator.removeFromSuperview()
        loadingView.removeFromSuperview()
    }

}
