//
//  PullRequestService.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit
import Alamofire
import Mantle

class PullRequestService: NSObject {

    func loadPullRequests(url: String, success:(([PullRequest]) -> Void)?, failure:((NSError) -> Void)?) {
        let headers = ["Content-Type" : "application/json"]
        Alamofire.request(.GET, url, parameters: nil, encoding: ParameterEncoding.JSON, headers: headers)
            .validate()
            .responseJSON { response in
                debugPrint(response)
                
                switch response.result {
                case .Success:
                    if let json = response.result.value as? [[String: AnyObject]] {
                        do {
                            let arrayOfPullRequests = try MTLJSONAdapter.modelsOfClass(PullRequest.self, fromJSONArray: json)
                            if success != nil {
                                success!(arrayOfPullRequests as! [PullRequest])
                            }
                        } catch {
                            print(error)
                            if failure != nil {
                                let error = ErrorHelper.createError(Error.Code.Undefined, message: Error.Message.Undefined)
                                failure!(error)
                            }
                        }
                    }
                case .Failure:
                    if failure != nil {
                        let error = ErrorHelper.createError(Error.Code.SearchPullRequests, message: Error.Message.SearchPullRequests)
                        failure!(error)
                    }
                }
        }

    }

    
}
