//
//  OwnerService.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit
import Alamofire
import Mantle

class OwnerService: NSObject {

    func loadOwnerInformation(url: String, success:((OwnerInformation) -> Void)?, failure: ((NSError!) -> Void)?) -> Void {
        let headers = ["Content-Type" : "application/json"]
        
        Alamofire.request(.GET, url, parameters: nil, encoding: ParameterEncoding.JSON, headers: headers)
            .validate()
            .responseJSON { response in
                debugPrint(response)
                
                switch response.result {
                case .Success:
                    if let json = response.result.value as? [String: AnyObject] {
                        do {
                            let information = try MTLJSONAdapter.modelOfClass(OwnerInformation.self, fromJSONDictionary: json)
                            if success != nil {
                                success!(information as! OwnerInformation)
                            }
                        } catch {
                            if failure != nil {
                                let error = ErrorHelper.createError(Error.Code.Undefined, message: Error.Message.Undefined)
                                failure!(error)
                            }
                        }
                    }
                case .Failure:
                    if failure != nil {
                        let error = ErrorHelper.createError(Error.Code.OwnerInformation, message: Error.Message.OwnerInformation)
                        failure!(error)
                    }
                }
        }
        
    }
    
}
