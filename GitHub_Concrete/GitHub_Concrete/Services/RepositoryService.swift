//
//  RepositoryService.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/9/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit
import Alamofire
import Mantle

class RepositoryService: NSObject {

    func loadRepositories(page: Int, success: (([Repository]) -> Void)?, failure: ((NSError!) -> Void)?) -> Void {
        let url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
        var repositories = [Repository]()
    
        let headers = ["Content-Type" : "application/json"]
        Alamofire.request(.GET, url, parameters: nil, encoding: ParameterEncoding.JSON, headers: headers)
            .validate()
            .responseJSON { response in
                debugPrint(response)
                
                switch response.result {
                case .Success:
                    if let json = response.result.value as? [String: AnyObject] {
                        for dictionary : [String : AnyObject] in json["items"] as! [[String: AnyObject]] {
                            do {
                                let repository = try MTLJSONAdapter.modelOfClass(Repository.self, fromJSONDictionary: dictionary)
                                repositories.append(repository as! Repository)
                            } catch {
                                print(error)
                            }
                        }
                        
                        if success != nil {
                            success!(repositories)
                        }
                    }
                case .Failure:
                    if failure != nil {
                        let error = ErrorHelper.createError(Error.Code.SearchRepositories, message: Error.Message.SearchRepositories)
                        failure!(error)
                    }
                }
        }
    }
}
