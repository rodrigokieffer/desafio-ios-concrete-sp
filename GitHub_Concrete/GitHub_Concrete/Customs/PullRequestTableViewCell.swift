//
//  PullRequestTableViewCell.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var labelPullRequestName: UILabel!
    @IBOutlet weak var imagePullRequestAvatar: UIImageView!
    @IBOutlet weak var labelPullRequestDescription: UILabel!
    @IBOutlet weak var labelPullRequestFullName: UILabel!
    @IBOutlet weak var labelPullRequestLogin: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let radiusImage = imagePullRequestAvatar.frame.size.width / 2
        imagePullRequestAvatar.layer.masksToBounds = true
        imagePullRequestAvatar.layer.cornerRadius  = radiusImage
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
