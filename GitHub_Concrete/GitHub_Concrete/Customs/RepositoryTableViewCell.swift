//
//  RepositoryTableViewCell.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/9/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var labelRepositoryName: UILabel!
    @IBOutlet weak var labelRepositoryDescription: UILabel!
    @IBOutlet weak var labelForks: UILabel!
    @IBOutlet weak var labelStars: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelFullname: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let radiusImage = imageAvatar.frame.size.width / 2
        imageAvatar.layer.masksToBounds = true
        imageAvatar.layer.cornerRadius  = radiusImage
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
