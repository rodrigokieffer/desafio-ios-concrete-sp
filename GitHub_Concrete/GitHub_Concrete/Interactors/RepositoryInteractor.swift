//
//  RepositoryInteractor.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit

class RepositoryInteractor: NSObject {

    func loadRepositories(page: Int, success: (([Repository]) -> Void)?, failure: ((NSError!) -> Void)?) -> Void {        
        let service = RepositoryService()
        var numberOfInformationDownloaded = 0
        service.loadRepositories(page, success: { (repositories) in
            let ownerService = OwnerService()
            for repository in repositories {
                ownerService.loadOwnerInformation(repository.owner.url, success: { (information) in
                    repository.owner.fullName = information.name
                    numberOfInformationDownloaded = numberOfInformationDownloaded + 1
                    if numberOfInformationDownloaded == repositories.count {
                        if success != nil {
                            success!(repositories)
                        }
                    }
                }, failure: { (error) in
                    print(error)
                    numberOfInformationDownloaded = numberOfInformationDownloaded + 1
                    if numberOfInformationDownloaded == repositories.count {
                        if success != nil {
                            success!(repositories)
                        }
                    }
                })
            }
        }, failure: { (error) in
            if failure != nil {
                failure!(error)
            }
        })    
    }
    
}
