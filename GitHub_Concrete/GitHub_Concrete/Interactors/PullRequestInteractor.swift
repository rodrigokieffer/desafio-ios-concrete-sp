//
//  PullRequestInteractor.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import UIKit

class PullRequestInteractor: NSObject {

    func loadPullRequests(url: String, success:(([PullRequest]) -> Void)?, failure:((NSError) -> Void)?) {
        let service = PullRequestService()
        var numberOfInformationDownloaded = 0
        service.loadPullRequests(url, success: { (pullRequests) in
            let ownerService = OwnerService()
            for pullRequest in pullRequests {
                ownerService.loadOwnerInformation(pullRequest.owner.url, success: { (information) in
                    pullRequest.owner.fullName = information.name
                    numberOfInformationDownloaded = numberOfInformationDownloaded + 1
                    if numberOfInformationDownloaded == pullRequests.count {
                        if success != nil {
                            success!(pullRequests)
                        }
                    }
                    }, failure: { (error) in
                        print(error)
                        numberOfInformationDownloaded = numberOfInformationDownloaded + 1
                        if numberOfInformationDownloaded == pullRequests.count {
                            if success != nil {
                                success!(pullRequests)
                            }
                        }
                })
            }
        }, failure: { (error) in
            if failure != nil {
                failure!(error)
            }
        })
    }
    
}
