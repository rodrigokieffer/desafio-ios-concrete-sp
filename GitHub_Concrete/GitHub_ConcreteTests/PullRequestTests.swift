//
//  PullRequestTests.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import XCTest
@testable import GitHub_Concrete

class PullRequestTests: XCTestCase {
    
    let pullUrl = "https://api.github.com/repos/JakeWharton/butterknife/pulls"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let helper = StubHelper()
        helper.loadPullRequests()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        StubHelper.removeAllStubs()
        super.tearDown()
    }
    
    func testPullsCount() {
        self.measureBlock {
            let expectation = self.expectationWithDescription("Alamofire")
            
            let interactor = PullRequestInteractor()
            interactor.loadPullRequests(self.pullUrl, success: { (arrayOfPulls) in
                XCTAssert(arrayOfPulls.count > 0)
                expectation.fulfill()
            }, failure: { (error) in
                print(error.localizedDescription)
            })
            
            self.waitForExpectationsWithTimeout(5.0, handler: nil)
        }
    }
    
    func testPullRequestFields() {
        measureBlock {
            let expectation = self.expectationWithDescription("Alamofire")
            
            let interactor = PullRequestInteractor()
            interactor.loadPullRequests(self.pullUrl, success: { (arrayOfPulls) in
                let pullRequest = arrayOfPulls[0]
                XCTAssert(pullRequest.title != "", "Pull request title is not empty")
                XCTAssert(pullRequest.pullDescription != "", "Pull request description is not empty")
                XCTAssert(pullRequest.owner.avatarUrl != "", "Pull request user avatar url is not empty")
                XCTAssert(pullRequest.owner.login != "", "Pull request user login url is not empty")
                
                expectation.fulfill()
                }, failure: { (error) in
                    print(error.localizedDescription)
            })
            
            self.waitForExpectationsWithTimeout(5.0, handler: nil)
        }
    }

    
}
