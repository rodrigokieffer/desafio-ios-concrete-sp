//
//  RepositoryTests.swift
//  GitHub_Concrete
//
//  Created by Rodrigo Kieffer on 9/10/16.
//  Copyright © 2016 Rodrigo Kieffer. All rights reserved.
//

import XCTest
@testable import GitHub_Concrete

class RepositoryTests: XCTestCase {
    
    var repositoryViewController: RepositoriesTableViewController!
    var arrayOfRepositories: [Repository]!
    
    override func setUp() {
        super.setUp()

        let helper = StubHelper()
        helper.loadRepositories()

        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        
        repositoryViewController = navigationController.viewControllers[0] as! RepositoriesTableViewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        StubHelper.removeAllStubs()
        super.tearDown()
    }
    
    func testRepositoryTable() {
        //Check if tableView is loaded
        XCTAssertNotNil(repositoryViewController.tableView)
    }
    
    func testRepositoriesCount() {
        measureBlock {
            let expectation = self.expectationWithDescription("Alamofire")

            let interactor = RepositoryInteractor()
            interactor.loadRepositories(1, success: { (arrayOfRepo) in
                XCTAssert(arrayOfRepo.count > 0)
                expectation.fulfill()
            }, failure: { (error) in
                print(error.localizedDescription)
            })
            
            self.waitForExpectationsWithTimeout(5.0, handler: nil)
        }
    }
    
    func testRepositoryFields() {
        measureBlock { 
            let expectation = self.expectationWithDescription("Alamofire")
            
            let interactor = RepositoryInteractor()
            interactor.loadRepositories(1, success: { (arrayOfRepo) in
                let repository = arrayOfRepo[0]
                XCTAssert(repository.repositoryName != "", "Repository name is not empty")
                XCTAssert(repository.repositoryDescription != "", "Repository description is not empty")
                XCTAssert(repository.numberOfForks != 0, "Repository forks is not zero")
                XCTAssert(repository.numberOfStars != 0, "Repository forks is not zero")
                XCTAssert(repository.owner.avatarUrl != "", "Repository owner avatar url is not empty")
                XCTAssert(repository.owner.login != "", "Repository owner login url is not empty")
                
                expectation.fulfill()
                }, failure: { (error) in
                    print(error.localizedDescription)
            })
            
            self.waitForExpectationsWithTimeout(5.0, handler: nil)
        }
    }
}
